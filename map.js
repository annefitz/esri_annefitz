$(document).ready(function(){
  // initialize map on page load
  require([
      "esri/Map",
      "esri/views/MapView",
      "esri/layers/Layer",
      "esri/Graphic",
      "esri/layers/GraphicsLayer",
      "dojo/domReady!"
    ], function(Map, MapView, Layer, Graphic, GraphicsLayer) {

    var map = new Map({
      basemap: "dark-gray"
    });

    // create a new map centered at the University of Dayton
    var view = new MapView({
      container: "viewDiv",
      map: map,
      center: [-84.1747,39.7377],
      zoom: 16
    });

    var graphLayer =  new GraphicsLayer();
    var buildings = getBldgArray();
    buildings.forEach(createPoint);

    // create a graphic for each building based on 'buildings' array
    function createPoint(value){
      var point = {
        type: "point",
        longitude: parseFloat(value.longitude),
        latitude: parseFloat(value.latitude)
      }
      var udSymbol = {
        type: "simple-marker",
        color: value.color,
        outline: {
          color: [255, 255, 255],
          width: 2
        }
      };
      var buildingAttr = {
        Building: value.building,
        Purpose: value.purpose
      }
      var UDpoint = new Graphic ({
        geometry: point,
        symbol: udSymbol,
        attributes: buildingAttr,
        popupTemplate: {
          title: "{Building}",
          content: [{
            type: "fields",
            fieldInfos: [{
            fieldName: "Building"
            }, {
              fieldName: "Purpose"
            }]
          }]
        }
      });
      // add each graphic to the graphics layer
      graphLayer.graphics.add(UDpoint);
    }
    // add graphics layer to map once all graphics are initialized
    map.add(graphLayer);

    // if a checkbox is clicked, runs the checkFilters() function
    $('input').on('click', function(){
      checkFilters();
    });
    // clears filters from map when button is clicked
    $('#filterBtn').on('click', function(){
      clearFilters();
    });

    // resets checkboxes and clears map of filters
    function clearFilters(){
      $("#residentialCbx").prop('checked', false);
      $("#academicCbx").prop('checked', false);
      $("#recreationalCbx").prop('checked', false);
      graphLayer.removeAll();
      getBldgArray().forEach(createPoint);
    }

    // checks which filter boxes have been clicked, then filters accordingly
    function checkFilters(){
      var resChbx = $("#residentialCbx").prop('checked');
      var acaChbx = $("#academicCbx").prop('checked');
      var recChbx = $("#recreationalCbx").prop('checked');

      if (resChbx || acaChbx || recChbx) {
        filterBuildings(resChbx, acaChbx, recChbx);
      } else {
        getBldgArray().forEach(createPoint);
      }
    }

    // clears current graphics from map and replaces with filtered graphics
    function filterBuildings(residential, academic, recreational){
      graphLayer.removeAll();
      var buildings = getBldgArray();
      var filteredArray = new Array();
      $.each(buildings, function(key,value){
        if ((residential && value.purpose == "Residential") || (academic && value.purpose == "Academic") || (recreational && value.purpose == "Recreational")) {
          filteredArray.push(value);
        }
      });
      filteredArray.forEach(createPoint);
    }
  });
});
