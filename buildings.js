// Creates new Building object when called
function Building(latitude, longitude, color, building, purpose){
  this.latitude = latitude;
  this.longitude = longitude;
  this.color = color;
  this.building = building;
  this.purpose = purpose;
}
// Used to create array of building objects to be displayed on the map
function getBldgArray(){
  var bldgList = new Array();
  bldgList.push(new Building("39.737789","-84.174247","orange","Marianist Hall","Residential"));
  bldgList.push(new Building("39.7365","-84.1740","blue","RecPlex","Recreational"));
  bldgList.push(new Building("39.7359","-84.1713","blue","Stuart Field","Recreational"));
  bldgList.push(new Building("39.7379","-84.1765","red","Kettering Labs","Academic"));
  bldgList.push(new Building("39.7386","-84.1766","red","J.P. Humanities Center","Academic"));
  bldgList.push(new Building("39.7387","-84.1777","red","St. Joseph's Hall","Academic"));
  bldgList.push(new Building("39.7358","-84.1699","orange","Stuart Hall","Residential"));
  bldgList.push(new Building("39.7372","-84.1708","orange","Virginia W. Kettering Hall","Residential"));
  bldgList.push(new Building("39.7385","-84.1750","orange","Founders Hall","Residential"));
  bldgList.push(new Building("39.7395","-84.1762","blue","Kennedy Union","Recreational"));
  bldgList.push(new Building("39.7401","-84.1751","red","Science Center","Academic"));
  bldgList.push(new Building("39.7394","-84.1729","orange","Marycrest Complex","Residential"));
  bldgList.push(new Building("39.7405","-84.1762","red","Miriam Hall","Academic"));
  bldgList.push(new Building("39.7402","-84.1788","red","Roesch Library","Academic"));
  bldgList.push(new Building("39.7387","-84.1822","red","Raymond L. Fitz Hall","Academic"));
  bldgList.push(new Building("39.7374","-84.1805","orange","Caldwell Apartments","Residential"));
  bldgList.push(new Building("39.7350","-84.1784","orange","Adele Center","Residential"));
  bldgList.push(new Building("39.7340","-84.1780","orange","Campus South Apartments","Residential"));
  return bldgList;
}
